from MenuBuilder import Console
from defaultMenues import *


def main():
    menu1 = NumericMenu("men1")
    menu2 = WordMenu("wordeludidoo")
    menu1.add_entity(menu2)
    menu3 = NumericMenu("1+1")

    main_menu = NumericMenu("Main")
    main_menu.parent = main_menu
    main_menu.add_entity(menu1)
    main_menu.add_entity(menu2)
    main_menu.add_entity(menu3)

    console = Console(main_menu)
    console.main()


if __name__ == '__main__':
    main()
