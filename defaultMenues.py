from MenuBuilder import Menu, Entity, PARENT_MENU


class NumericMenu(Menu):

    def is_valid(self, input_line: str) -> bool:
        try:
            return input_line == PARENT_MENU or int(input_line) < len(self.entities)
        except ValueError:
            return False

    def get_values(self):
        list_values = ""
        for i in range(len(self.entities)):
            list_values += f"{i} - {self.entities[i].name} \n"
        return list_values

    def get_sub_entity(self, name) -> Entity:
        if name == PARENT_MENU:
            return self.parent
        self.entities[int(name)].parent = self
        return self.entities[int(name)]


class WordMenu(Menu):
    """default implementation does it already"""
    pass
