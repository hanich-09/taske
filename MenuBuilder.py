from abc import ABC, abstractmethod

PS1 = ">"
PARENT_MENU = ".."


class Entity(ABC):

    def __init__(self, name):
        self.name = name

    @abstractmethod
    def get_values(self) -> str:
        pass

    @abstractmethod
    def is_valid(self, input_line: str) -> bool:
        pass


class Command(Entity):

    def __init__(self, name, arg_number, function):
        self.name = name
        self.arg_number = arg_number
        self.function = function

    def get_values(self) -> str:
        """return name of function and number of arguments it takes"""
        return f"{self.name} takes {self.arg_number} arguments"

    def is_valid(self, input_line: str) -> bool:
        """checks if the command name is in the input line,
         and the arguments match what the function expects"""
        split_line = input_line.split(" ")
        return split_line[0] == self.name and len(split_line) == self.arg_number - 1

    def run_command(self, input_line: str) -> str:
        """runs the command with the arguments given"""
        split_line = input_line.split(" ")
        return self.function(*split_line[1])


class Menu(Entity):
    """A menu containing entities inside it"""

    def __init__(self, name):
        self.name = name
        self.entities = []
        self.parent = None

    def add_entity(self, entity: Entity) -> None:
        """add entity to the menu, can be anything inheriting from Entity"""
        self.entities.append(entity)

    def get_values(self):
        """return the name of all the entities it contains"""
        list_values = ""
        for entity in self.entities:
            list_values += entity.name + "\n"
        return list_values

    def is_valid(self, input_line: str) -> bool:
        """check if the name is matching the input line"""
        for entity in self.entities:
            if input_line == entity.name:
                return True
        return input_line == PARENT_MENU

    def get_sub_entity(self, name) -> Entity:
        """return an Entity with the name given"""
        if name == PARENT_MENU:
            return self.parent
        for entity in self.entities:
            if entity.name == name:
                if isinstance(entity, Menu):
                    entity.parent = self
                return entity


class Console:

    def __init__(self, menu: Menu):
        """takes a menu to be the first one to be shown"""
        self.commands = []
        self.current_menu = menu
        self.output = ""

    def add_command(self, command: Command) -> None:
        """takes a command and enters it to the possible commands"""
        self.commands.append(command)

    def print_value(self):
        """returns the user an output matching the input he entered"""
        print(self.output)

    def run_input(self, line):
        """checks if the line entered is an actual entity in the console and runs it"""
        for entity in self.commands + [self.current_menu]:
            if entity.is_valid(line):
                if isinstance(entity, Command):
                    self.output = entity.run_command(line)
                    self.print_value()
                else:
                    self.current_menu = entity.get_sub_entity(line)

    def main(self):
        while True:
            self.output = self.current_menu.get_values()
            self.print_value()
            input_line = input(PS1)
            self.run_input(input_line)

